// https://nodewebapps.com/2017/11/04/getting-started-with-nodejs-and-kafka/
const uuid = require("uuid");
const {Producer, KafkaClient} = require('kafka-node');
const {TOPIC} = require('./consumer');
const {kafkaHost} = require('./../config/config');

const client = new KafkaClient({
    kafkaHost,
    sessionTimeout: 300,
    spinDelay: 100,
    retries: 2
});

const producer = new Producer(client, {requireAcks: 1});

producer.on("ready", () => {
    console.log("Kafka Producer is connected and ready.");
});

producer.on("error", (error) => {
    console.error('Error occurred', error);
});

const KafkaService = {
    sendRecord: (
        key, {
            application,
            userId,
            from,
            to,
            subject,
            body
        },
        callback = (error, result) => {

        }) => {
        if (!userId) {
            return callback(new Error(`A userId must be provided.`));
        }

        const event = {
            id: uuid.v4(),
            timestamp: Date.now(),
            application,
            userId,
            from,
            to,
            subject,
            body
        };

        const buffer = new Buffer.from(JSON.stringify(event));

        // Create a new payload
        const record = [{
            topic: TOPIC,
            messages: buffer,
            key,
            attributes: 2 /* Use GZip compression for the payload */
        }];

        //Send record to Kafka and log result/error
        producer.send(record, callback);
    }
};

module.exports = KafkaService;