const {ConsumerGroup} = require('kafka-node');
const {sendEmail} = require('./../routes/email-route');
const Email = require('./../models/email');
const {kafkaHost} = require('./../config/config');

// const argv = require('optimist').argv;
// const topic = argv.topic || 'webevents.sendEmail';
// const topics = [{topic, partition: 0}];

const TOPIC = 'webevents.sendEmail';
const ACTIVATION_EMAIL = 'ACTIVATION_EMAIL';
const RESET_PASSWORD_EMAIL = 'RESET_PASSWORD_EMAIL';

// bin/windows/kafka-topics.bat --create --zookeeper localhost:2181 --replication-factor 1 --partitions 3 --topic webevents.sendEmail
const options = {
    kafkaHost,
    groupId: 'node-group',
    // fromOffset: 'earliest',
    autoCommit: true,
    fetchMaxWaitMs: 1000,
    fetchMaxBytes: 1024 * 1024,
    encoding: 'buffer',
    keyEncoding: 'utf8',
    onRebalance: (isAlreadyMember, callback) => {
        console.log('on rebalance isAlreadyMember', isAlreadyMember);
        callback();
    }
};

const consumer = new ConsumerGroup(options, TOPIC);

consumer.on('message', (message) => {
    const {key, topic, highWaterOffset, offset, partition, value} = message;
    console.log(`message key ${key} topic ${topic} value ${value} offset ${offset} highWaterOffset ${highWaterOffset} partition ${partition}`);

    switch (key) {
        case ACTIVATION_EMAIL:
        case RESET_PASSWORD_EMAIL:
            sendEmail(_buildEmail(_decode(message.value)), (error, info) => {
                if (error) {
                    console.error(error);
                } else {
                    // todo send
                }
            });
            break;
        default:
    }

});

consumer.on('error', (err) => {
    console.error(`consumer error: `, err);
});

/*
* If consumer get `offsetOutOfRange` event, fetch data from the smallest(oldest) offset
*/
consumer.on('offsetOutOfRange', (topic) => {
    console.error('offsetOutOfRange', topic);
});

consumer.on('rebalancing', () => {
    console.log('rebalancing');
});

consumer.on('rebalanced', () => {
    console.log('rebalanced');
});

process.on("SIGINT", () => {
    consumer.close(true, () => {
        process.exit();
    });
});

const _decode = (value) => {
    const buf = new Buffer.from(value, "binary");
    return JSON.parse(buf.toString());
};

const _buildEmail = (value) => {
    return new Email.Builder()
        .withApplication(value.application)
        .withUserId(value.userId)
        .withFrom(value.from)
        .withTo(value.to)
        .withSubject(value.subject)
        .withBody(value.body)
        .build();
};

module.exports = {TOPIC, ACTIVATION_EMAIL};