const oracledb = require('oracledb');
const {user, password, connectString} = require('./config').dbConfig;

oracledb.getConnection({user, password, connectString}, (err, connection) => {
    if (err) {
        console.error(err.message);
        return;
    }
    console.log('Connection was successful!');

    connection.close((err) => {
        if (err) {
            console.error(err.message);
        }
    });
});