const expect = require('expect');
const request = require('supertest');
const {app} = require('../app');
const {from} = require('../config/config');
const Email = require('./../models/email');

const email = new Email.Builder()
    .withApplication('TEST_APP')
    .withUserId(3)
    .withFrom(from)
    .withTo('k.rozycki@onet.pl')
    .withSubject('test')
    .withBody('test')
    .build();

describe('POST /send-email', () => {

    it('should send an email', done => {
        request(app)
            .post('/send-email')
            .send(email)
            .expect('Content-Type', /json/)
            .expect(200)
            .expect((res) => {
                expect(res.body.info.rejected.length).toBe(0);
                expect(res.body.info.accepted.length).toBe(1);
            })
            .end((err, res) => {
                if (err)
                    return done(err);
                done();
            });
    });

    it('should not send an email empty body', done => {
        email.body = null;

        request(app)
            .post('/send-email')
            .send(email)
            .expect('Content-Type', /json/)
            .expect(400)
            .end((err, res) => {
                if (err) return done(err);
                done();
            });
    });

    it('should not send an email incorrect email', done => {
        email.body = 'incorrect email';
        email.to = 'marian.pl';

        request(app)
            .post('/send-email')
            .send(email)
            .expect('Content-Type', /json/)
            .expect(400)
            .end((err, res) => {
                if (err) return done(err);
                done();
            });
    });

});


