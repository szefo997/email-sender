const expect = require('expect');
// const {app} = require('../app');
const KafkaService = require('./../kafka/producer');
const {ACTIVATION_EMAIL, TOPIC} = require("../kafka/consumer");

describe('CONSUMER topic: webevents.sendEmail', () => {

    it('should send an email', done => {
        // userId, applicationId, from, to, subject

        KafkaService.sendRecord(ACTIVATION_EMAIL, {
            userId: 3,
            application: 'ERP_SABA',
            from: 'marian@pw.pl',
            body: '<h3>body consumer test</h3>',
            subject: 'subject test consumer',
            to: 'k.rozycki@onet.pl'
        }, (err, result) => {
            console.warn('result', result);

            if (err) return done(err);

            expect(err).toBe(null);
            expect(result[TOPIC]).toBeDefined();

            done();
        });

    });

});


