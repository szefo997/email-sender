export default class Email {

    constructor(build) {
        this.application = build.application;
        this.userId = build.userId;
        this.from = build.from;
        this.to = build.to;
        this.subject = build.subject;
        this.body = build.body;
    }

    static get Builder() {

        class Builder {

            constructor() {
            }

            withApplication(application) {
                this.application = application;
                return this;
            }

            withUserId(userId) {
                this.userId = userId;
                return this;
            }

            withFrom(from) {
                this.from = from;
                return this;
            }

            withTo(to) {
                this.to = to;
                return this;
            }

            withSubject(subject) {
                this.subject = subject;
                return this;
            }

            withBody(body) {
                this.body = body;
                return this;
            }

            build() {
                return new Email(this);
            }
        }

        return Builder;
    }
}
