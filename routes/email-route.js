const nodeMailer = require('nodemailer');
const {transportConfig} = require("../config/config");

const sendEmailReq = (req, res) => {
    const email = req.body;
    for (const key in email) {
        if (email.hasOwnProperty(key) && !email[key]) {
            return res.status(400).send({
                error: `All email fields are required key ${key}`
            });
        }
    }

    sendEmail(email, (error, info) => {
        if (error) {
            console.log(error);
            return res.status(400).send({error});
        }
        console.log('Message %s sent: %s', info.messageId, info.response);
        res.send({info});
    })
};

const sendEmail = (email,
                   callback = (error, info) => {
                   }) => {
    if (!email) {
        callback('Email is required');
        return;
    }

    for (const key in email) {
        if (email.hasOwnProperty(key) && !email[key]) {
            callback(`All email fields are required key ${key}`);
            return;
        }
    }

    const {to, subject, body, from} = email;

    const transporter = nodeMailer.createTransport(transportConfig);
    const mailOptions = {
        from,
        to,
        subject,
        text: body,
        html: body
    };

    transporter.sendMail(mailOptions, (error, info) => {
        callback(error, info);
    });
};

module.exports = {sendEmail, sendEmailReq};
